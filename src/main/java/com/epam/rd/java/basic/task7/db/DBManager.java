package com.epam.rd.java.basic.task7.db;

import java.util.*;
import java.util.logging.*;
import java.io.*;
import java.sql.*;


import com.epam.rd.java.basic.task7.db.entity.*;


public class DBManager {

    private static final Logger LOGGER = Logger.getLogger(DBManager.class.getName());

    private static DBManager instance;

    public static synchronized DBManager getInstance() {
        if (instance == null) {
            instance = new DBManager();
        }
        return instance;
    }

    private DBManager() {
    }

    private static final String URL;

    static {
        Properties prop = new Properties();
        String temp = "";
        try (InputStream input = new FileInputStream("app.properties")) {
            prop.load(input);
            temp = prop.getProperty("connection.url");
            LOGGER.fine("app.properties loaded");
        } catch (IOException e) {
            LOGGER.log(Level.SEVERE, "Failed loading app.properties: ", e);
        }
        URL = temp;
    }

    private static final String SQL_FIND_ALL_USERS = "SELECT * FROM users rs";
    private static final String SQL_FIND_ALL_TEAMS = "SELECT * FROM teams";
    private static final String SQL_INSERT_USER = "INSERT INTO users VALUES (DEFAULT, ?)";
    private static final String SQL_DELETE_USERS = "DELETE FROM users WHERE login=?";
    private static final String SQL_INSERT_TEAM = "INSERT INTO teams VALUES (DEFAULT, ?)";
    private static final String SQL_GET_USER = "SELECT id, login FROM users WHERE login=?";
    private static final String SQL_GET_TEAM = "SELECT id, name FROM teams WHERE name=?";
    private static final String SQL_INSERT_TEAM_FOR_USER = "INSERT INTO users_teams VALUES (?, ?)";
    private static final String SQL_GET_USER_TEAMS = "SELECT * FROM teams WHERE teams.id " +
            "= ANY (SELECT team_id FROM users_teams WHERE user_id=?)";
    private static final String SQL_DELETE_TEAM = "DELETE FROM teams WHERE name=?";
    private static final String SQL_UPDATE_TEAM = "UPDATE teams SET name=? WHERE id=?";

    public List<User> findAllUsers() throws DBException {
        List<User> users = new ArrayList<>();
        try {
            Connection con = DriverManager.getConnection(URL);
            Statement stmt = con.createStatement();
            ResultSet rs = stmt.executeQuery(SQL_FIND_ALL_USERS);
            while (rs.next()) {
                users.add(extractUser(rs));
            }
            users.sort(Comparator.comparingInt(User::getId));
        } catch (SQLException e) {
            throw new DBException("DB findAllUsers() problem", e);
        }
        return users;
    }

    public boolean insertUser(User user) throws DBException {
        boolean res = false;
        ResultSet rs = null;
        try (Connection connection = DriverManager.getConnection(URL);
             PreparedStatement statement =
                     connection.prepareStatement(SQL_INSERT_USER,
                             Statement.RETURN_GENERATED_KEYS)) {
            int k = 1;
            statement.setString(k, user.getLogin());

            if (statement.executeUpdate() > 0) {
                rs = statement.getGeneratedKeys();
                if (rs.next()) {
                    int id = rs.getInt(1);
                    user.setId(id);
                    res = true;
                }
            }
        } catch (SQLException e) {
            LOGGER.log(Level.SEVERE, "DB insertUser() problem", e);
            throw new DBException("DB insertUser() problem", e);
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException e) {
                    LOGGER.log(Level.SEVERE, "DB insertUser() -> rs.close() problem", e);
                }
            }
        }
        return res;
    }

    public boolean deleteUsers(User... users) throws DBException {
        boolean result = true;
        try (Connection con = DriverManager.getConnection(URL);
             PreparedStatement statement =
                     con.prepareStatement(SQL_DELETE_USERS)) {
            int k = 1;
            for (User u : users) {
                statement.setString(k, u.getLogin());
                result = result && statement.executeUpdate() > 0;
            }
        } catch (SQLException ex) {
            throw new DBException("DB deleteUsers() problem", ex);
        }
        return result;
    }

    public User getUser(String login) throws DBException {
        User user = null;
        ResultSet rs = null;
        try (Connection con = DriverManager.getConnection(URL);
             PreparedStatement statement =
                     con.prepareStatement(SQL_GET_USER)) {

            statement.setString(1, login);
            rs = statement.executeQuery();
            if (rs.next()) {
                user = extractUser(rs);
            }
        } catch (SQLException e) {
            LOGGER.log(Level.SEVERE, "getUser() SQLException ", e);
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException ex) {
                    throw new DBException("DB getUser() problem", ex);
                }
            }
        }
        return user;
    }

    public Team getTeam(String name) throws DBException {
        Team team = null;
        ResultSet rs = null;
        try (Connection con = DriverManager.getConnection(URL);
             PreparedStatement statement =
                     con.prepareStatement(SQL_GET_TEAM)) {

            statement.setString(1, name);
            rs = statement.executeQuery();
            if (rs.next()) {
                team = extractTeam(rs);
            }
        } catch (SQLException e) {
            LOGGER.log(Level.SEVERE, "getTeam() SQLException ", e);
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException ex) {
                    throw new DBException("DB getTeam() problem", ex);
                }
            }
        }
        return team;
    }

    public List<Team> findAllTeams() throws DBException {
        List<Team> teams = new ArrayList<>();

        try (Connection con = DriverManager.getConnection(URL);
             Statement stmt = con.createStatement();
             ResultSet rs = stmt.executeQuery(SQL_FIND_ALL_TEAMS)) {
            while (rs.next()) {
                teams.add(extractTeam(rs));
            }
        } catch (SQLException ex) {
            throw new DBException("DB findAllTeams() problem", ex);
        }
        return teams;
    }

    public boolean insertTeam(Team team) throws DBException {
        boolean res = false;
        ResultSet rs = null;
        try (Connection connection = DriverManager.getConnection(URL);
             PreparedStatement statement =
                     connection.prepareStatement(SQL_INSERT_TEAM,
                             Statement.RETURN_GENERATED_KEYS)) {
            int k = 1;
            statement.setString(k, team.getName());
            if (statement.executeUpdate() > 0) {
                rs = statement.getGeneratedKeys();
                if (rs.next()) {
                    int id = rs.getInt(1);
                    team.setId(id);
                    res = true;
                }
            }
        } catch (SQLException e) {
            LOGGER.log(Level.SEVERE, "DB insertTeam() problem ", e);
            throw new DBException("DB insertTeam() problem", e);

        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException e) {
                    LOGGER.log(Level.SEVERE, "DB insertTeam() -> rs.close() problem ", e);
                }
            }
        }
        return res;
    }

    public boolean setTeamsForUser(User user, Team... teams) throws DBException {
        Connection con = null;
        try {
            con = DriverManager.getConnection(URL);
            con.setAutoCommit(false);
            con.setTransactionIsolation(Connection.TRANSACTION_READ_COMMITTED);
            for (Team t : teams) {
                setTeamForUserCheck(con, user, t);
            }
            con.commit();
        } catch (SQLException ex) {
            if (con != null) {
                LOGGER.log(Level.SEVERE, "Cannot add user in two teams in method setTeamsForUser(): ", ex);
                try {
                    con.rollback();
                } catch (SQLException exs) {
                    throw new DBException("DB setTeamsForUser() problem", exs);
                }
            }
        } finally {
            if (con != null) {
                try {
                    con.close();
                } catch (SQLException ex) {
                    throw new DBException("DB setTeamsForUser() problem", ex);
                }
            }
        }
        return true;
    }

    private static boolean setTeamForUserCheck(Connection con, User user, Team team) throws SQLException {
        try {
            setTeamForUser(con, user, team);
        } catch (DBException ex) {
            LOGGER.log(Level.SEVERE, "Failed commit in method setTeamForUserCheck: ", ex);
            con.rollback();
            con.close();
            return false;
        }
        return true;
    }

    private static void setTeamForUser(Connection con, User user, Team team) throws DBException {
        try (PreparedStatement statement =
                     con.prepareStatement(SQL_INSERT_TEAM_FOR_USER)) {
            int k = 1;
            statement.setInt(k++, user.getId());
            statement.setInt(k, team.getId());
            statement.executeUpdate();

        } catch (SQLException e) {
            LOGGER.log(Level.SEVERE, "Failed in method setTeamForUser(): ", e);
            throw new DBException("DB setTeamForUser() problem", e);
        }
    }


    public List<Team> getUserTeams(User user) throws DBException {
        List<Team> teams = new ArrayList<>();
        ResultSet rs = null;
        try (Connection con = DriverManager.getConnection(URL);
             PreparedStatement statement = con.prepareStatement(SQL_GET_USER_TEAMS)) {

            statement.setInt(1, user.getId());
            rs = statement.executeQuery();
            while (rs.next()) {
                teams.add(extractTeam(rs));
            }
        } catch (SQLException ex) {
            LOGGER.log(Level.SEVERE, "Failed in method getUserTeams(): ", ex);
            throw new DBException("DB getUserTeams() problem", ex);
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException e) {
                    LOGGER.log(Level.SEVERE, "Failed in method getUserTeams() -> rs.close(): ", e);
                }
            }
        }
        return teams;
    }

    private static User extractUser(ResultSet rs) throws SQLException {
        User user = new User();
        user.setId(rs.getInt("id"));
        user.setLogin(rs.getString("login"));
        return user;
    }

    private static Team extractTeam(ResultSet rs) throws SQLException {
        Team team = new Team();
        team.setId(rs.getInt("id"));
        team.setName(rs.getString("name"));
        return team;
    }

    public boolean deleteTeam(Team team) throws DBException {
        boolean result;

        try (Connection con = DriverManager.getConnection(URL);
             PreparedStatement statement =
                     con.prepareStatement(SQL_DELETE_TEAM)) {
            int k = 1;
            statement.setString(k, team.getName());
            result = statement.executeUpdate() > 0;
        } catch (SQLException ex) {
            throw new DBException("DB deleteTeam() problem", ex);
        }
        return result;
    }

    public boolean updateTeam(Team team) throws DBException {
        boolean result;

        try {
            Connection con = DriverManager.getConnection(URL);
            PreparedStatement statement =
                    con.prepareStatement(SQL_UPDATE_TEAM);

            int k = 1;
            statement.setString(k++, team.getName());
            statement.setInt(k, team.getId());

            result = statement.executeUpdate() > 0;
        } catch (SQLException ex) {
            throw new DBException("DB updateTeam() problem", ex);
        }
        return result;
    }

}


